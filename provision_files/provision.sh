#!/bin/bash

echo "==========================================================================="
echo "LEMP Starter Ting - Provisioner 1.3-WP"
echo "An eligent.group invention; 'tend to make stuff better."
echo "==========================================================================="
echo
echo
echo "Updating apt-get repositories ============================================="
sudo apt-get update > /dev/null 2>&1
echo
echo "Preparing for MySQL Installation =========================================="
echo "username: root"
echo "password: 1337m4st3r"
sudo apt-get install -y debconf-utils > /dev/null 2>&1
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password 1337m4st3r"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password 1337m4st3r"
echo
echo "Installing MySQL =========================================================="
sudo apt-get install -y mysql-server > /dev/null 2>&1
echo
echo "Creating database 'wp' 'if not exists' ===================================="
mysql -u root -p1337m4st3r -e "create database if not exists wp"; 
echo
echo "Check if any suitable DB is inside the 'db' folder ========================"
if [ -f "/vagrant/db/import.sql" ]; then
	echo "Found import.sql, trying..."
	mysql -u root -p1337m4st3r wp < /vagrant/db/import.sql
	mv /vagrant/db/import.sql /vagrant/db/import_PROCESSED.sql
else
	echo "No file named import.sql was found in the 'db' folder"
fi
echo
echo "Installing PhpMyAdmin ====================================================="
if [ ! -f /var/www/public/pma ]; then
	sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/internal/skip-preseed boolean true"
	sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect"
	sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean false"
	sudo apt-get -y install phpmyadmin > /dev/null 2>&1
else
	echo "Skipping (found: /var/www/public/pma)"
fi
echo
echo "Symlinking PMA..."
sudo ln -s /usr/share/phpmyadmin /var/www/public/pma
echo
echo "Installing PHP-fpm and MySQL module ======================================="
sudo apt-get install -y php-fpm php-mysql > /dev/null 2>&1
echo
echo "Installing PHP cURL =======================================================" 
sudo apt-get install -y curl > /dev/null 2>&1
sudo apt-get install -y php-curl > /dev/null 2>&1
echo
echo "Installing PHP UFW ========================================================" 
sudo apt-get install -y ufw > /dev/null 2>&1
echo
echo "Installing PHP zip ========================================================" 
sudo apt-get install -y php-zip > /dev/null 2>&1
echo
echo "Installing unzip ==========================================================" 
sudo apt-get install -y unzip > /dev/null 2>&1
echo
echo "Installing Nginx =========================================================="
sudo apt-get install -y nginx > /dev/null 2>&1
echo
echo "Configuring Nginx ========================================================="
sudo rm -rf /etc/nginx/sites-available/default > /dev/null 2>&1
cp /vagrant/provision_files/nginx_vhost /etc/nginx/sites-available/default > /dev/null 2>&1
echo
if [ ! -d "/var/certs" ]; then
	mkdir /var/certs
fi
if [ ! -f "/var/certs/eligent-self-signed.key" ] || [ ! -f "/var/certs/eligent-self-signed.crt" ]; then
	echo "Creating self-signed SSL certificate ======================================"
	sudo openssl req -subj '/CN=notimportant.io/O=Goddo McFukket/C=DE' -new -newkey rsa:2048 -sha256 -days 365 -nodes -x509 -keyout /var/certs/eligent-self-signed.key -out /var/certs/eligent-self-signed.crt > /dev/null 2>&1
fi
echo
if [ ! -f "/var/certs/dhparam.pem" ]; then
	echo "Creating dhparam.pem (will take AGES but happens just once </3) ==========="
	echo
	sudo openssl dhparam -dsaparam -out /var/certs/dhparam.pem 2048 > /dev/null 2>&1
fi
echo
echo "Restarting Nginx =========================================================="
sudo service nginx restart > /dev/null 2>&1
echo
echo "Setting firewall: enable Nginx Full ======================================="
sudo ufw allow 'Nginx Full'
echo
# echo "Creating a standard user =================================================="
# echo "username: ubuntu"
# echo "password: vagrant"
# echo "ubuntu:vagrant" | chpasswd
# echo
echo "Checking if WordPress needs provision ====================================="
if [ ! -f /var/www/public/wp-content/index.php ]; then

	echo "LOL!!! TURNS OUT IT DOES!!!"
	echo
	echo "Downloading WordPress Latest =============================================="
	curl -O https://wordpress.org/latest.zip > /dev/null 2>&1
	echo
	echo "Unpacking WordPress ======================================================="
	unzip latest.zip -d /var/www/public > /dev/null 2>&1
	echo
	echo "Moving WordPress to da trap house ========================================="
	cp -R /var/www/public/wordpress/* /var/www/public/
	echo
	echo "Removing WordPress rap rivals ============================================="
	rm -rf /var/www/public/wordpress
	echo
	echo "Removing crappy themes ===================================================="
	rm -rf /var/www/public/wp-content/themes
	rm -rf /var/www/public/wp-content/mu-plugins
	echo
	echo "Providing WordPress ======================================================="
	cp -R /vagrant/provision_files/wp/* /var/www/public/wp-content

else 
	echo "No provision required eskere =============================================="
fi
echo
# echo "Setting up sendmail ======================================================="
# sudo apt-get install sendmail -y > /dev/null 2>&1
# sudo echo "include(\`/etc/mail/tls/starttls.m4')dnl" >> /etc/mail/sendmail.mc > /dev/null 2>&1
# sudo sendmailconfig -y > /dev/null 2>&1
# sudo service sendmail restart > /dev/null 2>&1
# echo
# echo "Creating a standard user =================================================="
# echo "username: ubuntu"
# echo "password: vagrant"
# echo "ubuntu:vagrant" | chpasswd
echo
echo "Cleaning up ==============================================================="
if [ -d "/var/www/html" ]; then
	sudo rm -r /var/www/html
fi
echo
echo "+=========================================================================+"
echo "|                              S U C C E S S                              |"
echo "+=========================================================================+"






