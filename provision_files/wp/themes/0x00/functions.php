<?php

//////////////////////////////////////////////////////////////////
// Timber activation check.
if (!class_exists("Timber")) {
	add_action("admin_notices", function() {
			echo "<div class='error'><p>Timber not activated.</p></div>";
		});
	return;
}

//////////////////////////////////////////////////////////////////
// Tell Timber where to fimd the .twig files
Timber::$dirname 	= 	["views"];


//////////////////////////////////////////////////////////////////
// Main Timber Class - for timber related stuff
class Init0x00 extends TimberSite {

	function __construct() {

		// Add stupport for stuff
		add_theme_support("post-formats");
		add_theme_support("post-thumbnails");
		add_theme_support("menus");
		add_theme_support("custom-logo");

		/////////////////////////////////////////////////////////
		// Setup filters:

		// Allows to add anything to the $context
		add_filter("timber_context", [$this, "theme_addToContext"]);

		// Allows to extend twig; activate/deactivate modules
		add_filter("get_twig", [$this, "theme_addToTwig"]);

		// Allows to enqueue scripts and styles within the theme
		add_action("wp_enqueue_scripts", [$this, "theme_registerScripts"]);

		// Allows to register any menu
		add_action("after_setup_theme", [$this, "register_menu"]);

		////////////////////////////////////////////////////////
		// Call parent class' construct
		parent::__construct();
	}

	////////////////////////////////////////////////////////////
	// Allows to add anything to the $context
	function theme_addToContext($context){

		// Menus setup
		$context["menus"]["primary"]		= 	new TimberMenu("primary");
		$context["menus"]["footer"] 		= 	new TimberMenu("footer");
		$context["menus"]["mobile"] 		=	new TimberMenu("mobile");

		// General setup
		$context["body_class"] 				.= 	"";
		$context["site"]	 				= 	$this; // not sure if to keep this

		// This is set from the theme customiser - site identity
		$context["logo"]					=	get_custom_logo();
	
		return $context;
	}


	////////////////////////////////////////////////////////////
	// Allows to extend twig; activate/deactivate modules
	function theme_addToTwig($twig) {
		$twig->addExtension(new Twig_Extension_StringLoader());
		$twig->addExtension(new Twig_Extension_Debug());
		return $twig;
	}

	///////////////////////////////////////////////////////////
	// Allows to enqueue scripts and styles within the theme
	function theme_registerScripts() {

		// Quickly defining this theme's folder's path
		$theme_dir	=	get_bloginfo("stylesheet_directory");

		// Version - change this value to what you need
		$ver 	=	time(); // good for development

		// Register scripts block
		wp_register_script("main", "$theme_dir/js/site.js", ["jquery"], $ver, true);
		// wp_register_script("particlejs", "$theme_dir/js/particle.js", ["jquery"], $ver, true);
		// wp_register_script("smooth-scroll", "$theme_dir/js/smoothScroll.js", ["jquery"], $ver, true);
		// wp_register_script("scroll-reveal", "https://unpkg.com/scrollreveal@3.4.0/dist/scrollreveal.min.js", ["jquery"], $ver, true);
		
		// Enqueue scripts block
		// wp_enqueue_script("particlejs");
		// wp_enqueue_script("smooth-scroll");
		wp_enqueue_script("main");
		// wp_enqueue_script("scroll-reveal");

	}


	///////////////////////////////////////////////////////////
	// Allows to register any menu
	function register_menu() {
	  	register_nav_menu("primary", __("Primary Menu", "theme-slug"));
	  	register_nav_menu("footer", __("Footer Menu", "theme-slug"));
	 	register_nav_menu("mobile", __("Mobile Menu", "theme-slug"));
	}
}

// Instantiate theme's class
new Init0x00();


