<?php
/**
 * Search results page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$templates = array( 'search.twig', 'archive.twig', 'index.twig' );
$context = Timber::get_context();
$args = array_merge( $wp_query->query_vars, array( 'has_password' => false , 'post_status' => 'publish'  ) );
//$context['title'] = 'Search results for '. get_search_query();
$context['posts'] = Timber::get_posts($args);

Timber::render( $templates, $context );
